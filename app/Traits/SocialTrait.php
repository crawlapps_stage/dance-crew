<?php
namespace App\Traits;

use Illuminate\Support\Facades\Http;
use Twilio\Rest\Client;

trait SocialTrait{
    /**
     * Sends sms to user using Twilio's programmable sms client
     * @param String $message Body of sms
     * @param Number $recipients string or array of phone number of recepient
     */
    private function sendTwilioMessage($message, $recipients)
    {
        $recipients = '+' . str_replace("+","",$recipients);

        $client = new Client(env("TWILIO_SID"), env("TWILIO_AUTH_TOKEN"));
        $result = $client->messages->create($recipients,
            ['from' => env("TWILIO_NUMBER"), 'body' => $message] );
    }
}
