<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\SocialTrait;
use Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;
use Laravel\Passport\Client as OClient;

class UserApiController extends Controller
{
    use SocialTrait;
    public function sendOtp(Request $request){
        try{
            $type = ( Request::exists('email') ) ? 'email' : 'phone';
            $inputs = $request::all();
            $rules = [
                'device_id' => 'required',
                $type => "required|unique:users,$type"
            ];

            if( $type == 'phone' ){
                $rules['country_code'] = 'required';
            }
            $validator = Validator::make($request::all(), $rules);

            if ($validator->fails()) {
                return response()->json(['error' => $validator->messages() ], 422);
            }

            $is_exist_user = User::where('device_id', $inputs['device_id'])->first();

            $code = rand(1258, 5896);

            if( !$is_exist_user ){
                $inputs['otp'] = $code;
                $user = User::create($inputs);
            }else{
                $is_exist_user->$type = $inputs[$type];
                $is_exist_user->otp = $code;
                $is_exist_user->save();
            }
            $data['code'] = $code;
            $data['user_id'] = ( !$is_exist_user ) ? $user->id : $is_exist_user->id;
            return response()->json(['data' => $data], 200);
        }catch( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function verifyOtp(Request $request){
        try{
            $type = ( Request::exists('email') ) ? 'email' : 'phone';
            $inputs = $request::all();

            $validator = Validator::make($request::all(), [
                'otp' => 'required',
                'user_id' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['error' => $validator->messages()], 422);
            }
            $user = User::where('id', $inputs['user_id'])->where('otp', $inputs['otp'])->first();
            if( $user ){
                $user->otp = null;
                $user->verified_at = date('Y-m-d H:i:s');
                $user->save();

                $data['user_id'] = $user->id;
                $data['message'] = 'User verified successfully';
                return response()->json(['data' => $data], 200);
            }else{
                return response()->json(['error' => ['message' => 'Invalid otp.please try again' ] ], 422);
            }

        }catch( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function getAvtar(Request $request){
        try{
            $avtar = [
                ['id' => 1,'image' => asset('/images/avtar/avtar_1.png')],
                ['id' => 2,'image' => asset('/images/avtar/avtar_2.png')],
                ['id' => 3,'image' => asset('/images/avtar/avtar_3.png')],
                ['id' => 4,'image' => asset('/images/avtar/avtar_4.png')],
                ['id' => 5,'image' => asset('/images/avtar/avtar_5.png')],
            ];
            return response()->json(['data' => $avtar], 200);
        } catch(\Exception $e) {
            return response()->json(['error' => ['message' => "User not found..." ] ], 422);
        }
    }

    public function register(Request $request)
    {
        try {
            $rules = [
                'avtar' => 'required',
                'username'=> "required",
                'password' => 'required',
                'confirm_password' => 'required|same:password'
            ];
            $validator = Validator::make($request::all(), $rules);
            if ($validator->fails()) {
                return response()->json(['error' =>  $validator->messages() ], 422);
            }
            $inputs = $request::all();

            $user = User::find($inputs['user_id']);

            if( $user ){
                $email = 'user' . $user->id . date('Ymdhis') . '@dancecrew.com';
                $user->email = ( $user->email ) ? $user->email : $email;
                $user->username = $inputs['username'];
                $user->firstname = $inputs['username'];
                $user->password = bcrypt($inputs['password']);
                $user->avtar = $inputs['avtar'];
                $user->save();

//                $data['accessToken'] = $user->createToken('User:: ' . $user->username)->accessToken;
                $data['user_id'] = $user->id;
                $data['username'] = $user->username;

                $token = (array)$this->getTokenAndRefreshToken( $user->email, $inputs['password']);

                $data = array_merge($data, $token);
                return response()->json(['data' => $data], 200);
            }else{
                return response()->json(['error' => ['message' => 'User not found...' ] ], 422);
            }
        } catch(Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function login(Request $request){
        try{
            $type = ( Request::exists('email') ) ? 'email' : 'phone';
            $inputs = $request::all();
            $rules = ['password' => 'required',$type => 'required'];

            $validator = Validator::make($request::all(), $rules);

            if($validator->fails())
                return response()->json(['error' => $validator->messages()], 422);

            $user =  User::where('phone', $inputs['phone'])->first();

            if( $user ){
                $email = ( $type == 'email' ) ? $inputs['email'] : User::where('phone', $inputs['phone'])->first()->pluck('email');

                $credentials = ['email' => $email,'password' => $inputs['password']];

                if (!Auth::attempt($credentials))
                    return response()->json(['error' => ['message' => 'Email id or Password entered incorrect.' ] ], 422);
                else{
                    $user = Auth::user();
                    $data['user_id'] = $user->id;
                    $data['username'] = $user->username;

                    $token = (array)$this->getTokenAndRefreshToken( $user->email, $inputs['password']);

                    $data = array_merge($data, $token);

                    return response()->json(['data' => $data], 200);
                }
            }else{
                return response()->json(['error' => ['message' => 'User not found...'] ], 422);
            }
        }catch( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        try{
            $inputs = $request::all();
            if( !$inputs['user_id'] || $inputs['user_id'] == ''){
                return response()->json(['error' => ['message' => 'User not found...' ] ], 422);
            }
            $user = User::find($inputs['user_id']);

            if( !$user ){
                return response()->json(['error' => ['message' => 'User not found...' ] ], 422);
            }else{
                $tokens = $user->tokens;
                foreach($tokens as $token) {
                    $token->revoke();
                }
                return response()->json(['data' => 'logout successfully'], 200);
            }
        }catch( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ]], 422);
        }

    }

    /**
     * token generate
     * */
    public function getTokenAndRefreshToken($email, $password)
    {
        $oClient = OClient::where('password_client', 1)->first();
        $data = [
            'grant_type' => 'password',
            'client_id' => $oClient->id,
            'client_secret' => $oClient->secret,
            'username' => $email,
            'password' => $password,
            'scope' => '*',
        ];
        $response = \Illuminate\Http\Request::create('/oauth/token', 'POST', $data);
        $response = app()->handle($response);

        // Get the data from the response
        $data = json_decode($response->getContent());
        return $data;
    }

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function getnewtoken(Request $request)
    {
        try {
            $inputs = $request::all();
            $refresh_token = $inputs['refresh_token'];

            if (empty($refresh_token)) {
                return response()->json(['error' => ['message' => "Refresh token required..." ] ], 422);
            }

            $oClient = OClient::where('password_client', 1)->first();
            $data = [
                'grant_type' => 'refresh_token',
                'refresh_token' => $refresh_token,
                'client_id' => $oClient->id,
                'client_secret' => $oClient->secret,
                'scope' => '*',
            ];
            $response = \Illuminate\Http\Request::create('/oauth/token', 'POST', $data);
            $response = app()->handle($response);
            $data = json_decode($response->getContent());
            if($response->getStatusCode() == 401){
                return response()->json(['error' => ['message' => $data] ], 422);
            }else{
                return response()->json(['data' => $data], 200);
            }
        } catch
        (Exception $e) {
            return response()->json(['error' => ['message' => "Device not found..."] ], 422);
        }
    }

    public function invalidAuth()
    {
        return response()->json(['error' => ['message' => 'Unauthorized' ]], 401);
    }

    public function testTwilio(Request $request){
        $inputs = $request::all();

        $response = $this->sendTwilioMessage($inputs['message'], $inputs['recipient']);
        return response()->json(['data' => $response], 200);
    }
}
