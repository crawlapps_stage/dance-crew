import DashboardLayout from "../components/layouts/dashboard/DashboardLayout.vue";

// GeneralViews
import NotFound from "../components/pages/NotFoundPage.vue";

// Admin pages
import Dashboard from "../components/pages/Dashboard.vue";
import Avtar from "../components/pages/Avtar/Avtar.vue";
import UserProfile from "../components/pages/UserProfile.vue";
import Notifications from "../components/pages/Notifications.vue";
import Icons from "../components/pages/Icons.vue";
import Maps from "../components/pages/Maps.vue";
import Typography from "../components/pages/Typography.vue";
import TableList from "../components/pages/TableList.vue";
import AddAvtar from "../components/pages/Avtar/AddAvtar";

const routes = [
    {
        path: "/",
        component: DashboardLayout,
        redirect: "/dashboard",
        children: [
            {
                path: "dashboard",
                name: "dashboard",
                component: Dashboard
            },
            {
                path: "avtar",
                name: "avtar",
                component: Avtar
            },
            {
                path: "add-avtar",
                name: "add-avtar",
                component: AddAvtar
            },
            {
                path: "table-list",
                name: "table-list",
                component: TableList
            },

        ]
    },
    { path: "*", component: NotFound }
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
 function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;
