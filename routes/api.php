<?php

use App\Http\Controllers\Api\UserApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//
//});

Route::group(['prefix' => 'user', 'namespace' => 'Api'], function () {
    Route::post('send-otp', [UserApiController::class, 'sendOtp'])->name('send-otp');
    Route::post('verify-otp', [UserApiController::class, 'verifyOtp'])->name('verify-otp');
    Route::get('get-avtar', [UserApiController::class, 'getAvtar'])->name('get-avtar');
    Route::post('register', [UserApiController::class, 'register'])->name('register');
    Route::post('login', [UserApiController::class, 'login'])->name('login');
    Route::get('invalid-auth', [UserApiController::class, 'invalidAuth'])->name('invalid-auth');
    Route::post('get-new-token', [UserApiController::class, 'getnewtoken'])->name('get-new-token');

    Route::group([ 'middleware' => ['auth:api']], function() {
        Route::get('logout', [UserApiController::class, 'logout'])->name('logout');
    });

//    social testing
    Route::get('send-twilio-message', [UserApiController::class, 'testTwilio'])->name('send-twilio-message');
});
